# -*- coding: utf-8 -*-

from django.db import transaction
@transaction.commit_manually
def e():
		from datban import models
		reload (models)
		try:
			us = models.AdminUnit.objects.all()
			for u in us:
				u.slug = ''
				u.save()
			for u in us:
				print u.slug, len(u.markup), u.markup
		except Exception ,e :
			transaction.rollback()
		else:
			transaction.commit()
			
def t():
	from datban import models
	o = models.AdminUnit.objects.all().order_by('id')
	for u in o:
		if u.level ==4:
			print u.id,u.name
			#u.delete()

from django.db import transaction
@transaction.commit_manually
def l():
	from datban import models
	try:
		o = models.AdminUnit.objects.all().order_by('id')
		for u in o.filter(superordinate=49):
			print u.id,u.name
			u.slug=None
			u.save()
	except Exception, e:
		print "Rolled back."
		transaction.rollback()
		raise e
	else:
		transaction.commit()
		
from datban.models import AdminUnit
for u in AdminUnit.objects.filter(superordinate=49): print u.id, u.name, u.slug, u.path, u.markup


from django.db import transaction
@transaction.commit_manually
def d():
	from datban.models import Land, AdminUnit
	try:
		ol = Land.objects.all().order_by('id')
		for l in ol:
			ou = AdminUnit.objects.filter(geometry__intersects=l.geometry).exclude(id=2)
			print l.id, l.user
			max_level =0
			max_area=0
			max = None
			for u in ou:
				g =  l.geometry.intersection(u.geometry)
				g.transform(900913)
				if (u.level == max_level and g.area > max_area) or u.level > max_level:
					max_level = u.level
					max_area = g.area
					max=u
			if max:
				print '==>', max.id, max.name, max_level, max_area
				l.admin_unit = max
				l.save()
			else:
				raise Exception('Internal error: cannot intersect land with any admin unit')
	except Exception, e:
		print "Rolled back."
		transaction.rollback()
		raise e
	else:
		transaction.commit()

from django.db import transaction
@transaction.commit_manually		
def a():
	from datban.models import Land, AdminUnit
	try:
		ol = Land.objects.all().order_by('id')
		for l in ol:
			g = l.geometry.clone()
			g.transform(900913)
			l.area = g.area
			print l.id, l.user, l.area
			l.save()
	except Exception, e:
		print "Rolled back."
		transaction.rollback()
		raise e
	else:
		transaction.commit()

print random_date("1/1/2008 1:30 PM", "1/1/2009 4:50 AM", random.random())

		
from django.db import transaction
import traceback	
@transaction.commit_manually		
def m():
	startDate = "1/1/2007 1:00 AM"
	endDate = "9/1/2011 1:00 AM"
	from datban.models import Land, AdminUnit
	from datban import utils
	from datetime import datetime
	import random 
	try:
		ol = Land.objects.all().order_by('id')
		for l in ol:
			l.last_modified = datetime.fromtimestamp(time.mktime(utils.random_date(startDate,endDate, random.random())))
			l.save(keep_date=True)
			print l.id, l.user, l.last_modified
	except Exception, e:
		print "Rolled back."
		transaction.rollback()
		print "=" * 20
		traceback.print_exc()
		print "=" * 20
		raise e
	else:
		transaction.commit()
		
startDate = "1/1/2007 1:00 AM"
endDate = "9/1/2011 1:00 AM"
from datban.models import Land, AdminUnit
from datban import utils
import random 
t=utils.random_date(startDate,endDate, random.random())

from datban.models import Land
ol = Land.objects.all().filter(admin_unit__slug = 'xaphuocloc' )
for l in ol:
	print l.id, l.user, l.last_modified


startDate = "9/15/2011 1:00 AM"
endDate = "9/2/2011 1:00 AM"
from datban.models import Land
from datetime import datetime
from datban import utils
import time, random
ol = Land.objects.all().filter(admin_unit__slug = 'xaphuocloc' )
for l in ol:
	l.last_modified = datetime.fromtimestamp(time.mktime(utils.random_date(startDate,endDate, random.random())))
	l.save(keep_date = True)
	print l.id, l.user, l.last_modified

from datban.models import Land		
ol = Land.objects.all()
for l in ol:
	l.is_private = False
	l.save(keep_date = True)
	print l.id, l.user, l.last_modified, l.is_private


from django.db import transaction
import traceback	
@transaction.commit_manually		
def a():
	from datban.models import Land, AdminUnit
	try:
		ol = Land.objects.all().order_by('id')
		for l in ol:
			if l.user.username == 'nguyenivan':
				print l.id, l.contact_name, l.contact_phone
				#l.contact_name = u'Anh Nguyên'
				#l.contact_phone = '01635970520'
				#l.save()
	except Exception, e:
		print "Rolled back."
		transaction.rollback()
		traceback.print_exc()
		raise e
	else:
		transaction.commit()
		
from datban.models import Land	
ol = Land.objects.filter(id__gt=6110)
for l in ol:
	if l.admin_unit:
		print l.admin_unit.slug

startDate = "9/15/2011 1:00 AM"
endDate = "9/2/2009 1:00 AM"
import time, random
from datban.models import Land
from django.contrib.auth.models import User
from datetime import datetime
from datban import utils
def a():		
	ou = User.objects.all()
	users = []
	for u in ou:
		users.append(u)
	if len(users)>0:
		ol = Land.objects.all().filter(id__gt=2)
		for l in ol:
			l.last_modified = datetime.fromtimestamp(time.mktime(utils.random_date(startDate,endDate, random.random())))
			l.user = users[random.randint(0,len(users)-1)]
			l.contact_name = l.user.get_profile().name
			l.save(keep_date = True)
			print l.id, l.user, l.last_modified, l.contact_name
	else:
		print "=" * 20
		print "No user found!"
		print "=" * 20

from datban.models import AdminUnit
def a(commit=False):
	new_names = {
		811:u"Xã Quy Đức",
		812:u"Xã Bình Chánh",
		813:u"Xã Tân Quý Tây",
		814:u"Xã Hưng Long",
		815:u"Thị Trấn Tân Túc",
		816:u"Xã An Phú Tây",
		817:u"Xã Phong Phú",
		818:u"Xã Tân Kiên",
		819:u"Xã Bình Hưng",
		820:u"Xã Tân Nhựt",
		821:u"Xã Lê Minh Xuân",
		822:u"Xã Bình Lợi",
		823:u"Xã Vĩnh Lộc B",
		824:u"Xã Vĩnh Lộc A",
		825:u"Xã Phạm Văn Hai",
		826:u"Xã Đa Phước",
	}
	ou = AdminUnit.objects.all()
	for u in ou:
		if u.id in new_names:
			print "Updating ", u.id
			u.name=new_names[u.id]
			if commit:
				u.save()
			print u.id, u.name, u.path, u.slug
"""
function inspect(obj, maxLevels, level)
{
  var str = '', type, msg;

    // Start Input Validations
    // Don't touch, we start iterating at level zero
    if(level == null)  level = 0;

    // At least you want to show the first level
    if(maxLevels == null) maxLevels = 1;
    if(maxLevels < 1)     
        return '<font color="red">Error: Levels number must be > 0</font>';

    // We start with a non null object
    if(obj == null)
    return '<font color="red">Error: Object <b>NULL</b></font>';
    // End Input Validations

    // Each Iteration must be indented
    str += '<ul>';

    // Start iterations for all objects in obj
    for(property in obj)
    {
      try
      {
          // Show "property" and "type property"
          type =  typeof(obj[property]);
          str += '<li>(' + type + ') ' + property + 
                 ( (obj[property]==null)?(': <b>null</b>'):('')) + '</li>';

          // We keep iterating if this property is an Object, non null
          // and we are inside the required number of levels
          if((type == 'object') && (obj[property] != null) && (level+1 < maxLevels))
          str += inspect(obj[property], maxLevels, level+1);
      }
      catch(err)
      {
        // Is there some properties in obj we can't access? Print it red.
        if(typeof(err) == 'string') msg = err;
        else if(err.message)        msg = err.message;
        else if(err.description)    msg = err.description;
        else                        msg = 'Unknown';

        str += '<li><font color="red">(Error) ' + property + ': ' + msg +'</font></li>';
      }
    }

      // Close indent
      str += '</ul>';

    return str;
}

"""