from django.conf.urls.defaults import *
from django.conf import settings
from django.views.generic.simple import direct_to_template


urlpatterns = patterns('',
	#url(r'^$', direct_to_template, { 'template': 'datban/datbanhome.html', }, name = 'home'),
	url(r'^$', "datban.views.home" , name = "home"),
	url(r'^lands/(?P<unit_path>.*?)/$', "datban.views.lands", name = "lands"),
	url(r'^bando/(?P<unit_path>.*?)$', "datban.views.map", name = "map"),
	# Web services
	url(r'^coordtrans/$', "datban.views.coordtrans", name = "coordtrans"),
	url(r'^reversecoordtrans/$', "datban.views.reversecoordtrans", name = "reversecoordtrans"),
	url(r'^landws/$', "datban.views.landws", name = "landws"),
	url(r'^land_info/$', "datban.views.land_info", name = "land_info"),
	url(r'^test$', direct_to_template , {'template': 'datban/test.html'}),
	url(r'^edit/$', 'datban.views.land_edit', name='land_edit'),
)
