# -*- coding: utf-8 -*-
from __future__ import with_statement
from osgeo import ogr,osr
import numpy,sys


def vn2000_to_kml(wkt):
    vn2000 = osr.SpatialReference()
    vn2000.ImportFromProj4("+proj=tmerc +lat_0=0 +lon_0=105.75 +k=0.9999 +x_0=500000 +y_0=0 +ellps=WGS84 +units=m +no_defs +towgs84=-191.90441429,-39.30318279,-111.45032835,-0.00928836,0.01975479,-0.00427372,0.252906278")
    wgs84 = osr.SpatialReference()
    wgs84.ImportFromEPSG(900913)
    
    geom = ogr.CreateGeometryFromWkt ( wkt )
    geom.AssignSpatialReference ( vn2000 )
    geom.TransformTo ( wgs84 )
    
    kml = geom.ExportToKML()
    return kml

def vn2000_to_wkt(wkt): #Convert wkt in vn2000 to EPSG 4326 (WGS 84)
	vn2000 = osr.SpatialReference()
	vn2000.ImportFromProj4("+proj=tmerc +lat_0=0 +lon_0=105.75 +k=0.9999 +x_0=500000 +y_0=0 +ellps=WGS84 +units=m +no_defs +towgs84=-191.90441429,-39.30318279,-111.45032835,-0.00928836,0.01975479,-0.00427372,0.252906278")
	wgs84 = osr.SpatialReference()
	wgs84.ImportFromEPSG(900913)
	
	geom = ogr.CreateGeometryFromWkt ( wkt )
	geom.AssignSpatialReference ( vn2000 )
	geom.TransformTo ( wgs84 )
	
	converted = geom.ExportToWkt ()
	return converted

def epsg900913_vn2000_wkt(wkt): #Convert wkt from EPSG 900913 (WGS 84) to vn2000
	vn2000 = osr.SpatialReference()
	vn2000.ImportFromProj4("+proj=tmerc +lat_0=0 +lon_0=105.75 +k=0.9999 +x_0=500000 +y_0=0 +ellps=WGS84 +units=m +no_defs +towgs84=-191.90441429,-39.30318279,-111.45032835,-0.00928836,0.01975479,-0.00427372,0.252906278")
	wgs84 = osr.SpatialReference()
	wgs84.ImportFromEPSG(900913)
	
	geom = ogr.CreateGeometryFromWkt ( wkt )
	geom.AssignSpatialReference ( wgs84 )
	geom.TransformTo ( vn2000 )
	
	converted = geom.ExportToWkt ()
	return converted

def vn2000_to_geom(wkt):
	vn2000 = osr.SpatialReference()
	vn2000.ImportFromProj4("+proj=tmerc +lat_0=0 +lon_0=105.75 +k=0.9999 +x_0=500000 +y_0=0 +ellps=WGS84 +units=m +no_defs +towgs84=-191.90441429,-39.30318279,-111.45032835,-0.00928836,0.01975479,-0.00427372,0.252906278")
	wgs84 = osr.SpatialReference()
	wgs84.ImportFromEPSG(900913)
	
	geom = ogr.CreateGeometryFromWkt ( wkt )
	geom.AssignSpatialReference ( vn2000 )
	geom.TransformTo ( wgs84 )

	return geom

def m_to_d(wkt):
	m = osr.SpatialReference()
	m.ImportFromEPSG(900913)
	d = osr.SpatialReference()
	d.ImportFromEPSG(4326)
	
	geom = ogr.CreateGeometryFromWkt ( wkt )
	geom.AssignSpatialReference ( m )
	geom.TransformTo ( d )
	
	return geom

def add_html(polygon_kml, land_info):
    html = u'<table id="mytable" cellspacing="0" summary="Thông tin vị trí thửa đất trong sổ đỏ"> <caption>Thông tin vị trí thửa đất trong sổ đỏ</caption> <tr> <th scope="col"class="nobg">Thửa Đất</th> <th scope="col">Thông Tin</th> </tr> <tr> <td>Người Bán</td> <td>%s</td> </tr> <tr> <td>Số Điện Thoại</td> <td>%s</td> </tr> <tr> <td>Giá Bán</td> <td>%s</td> </tr> </table>' \
        % (land_info['name'], land_info['phone'], land_info['price'])
    
    kml = u'<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"><Document><Placemark><name><![CDATA[Tài sản đăng bán bởi: %s]]></name><description><![CDATA[%s]]></description><Style><IconStyle><scale>1.2</scale></IconStyle><LineStyle><color>ff00aaff</color><width>5</width></LineStyle><PolyStyle><color>33ffffff</color></PolyStyle></Style>%s</Placemark></Document></kml>' \
        % (land_info['name'],html,polygon_kml)
        
    return kml
