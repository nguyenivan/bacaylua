var map;
var polyline;

//Declare class
function LandManager(){
	var tile_hashtable = {};

	remove_tile = function(id){
		// Unset the overlay polyline features first
		for (var j = tile_hashtable[id].length - 1; j >= 0; j--){
			tile_hashtable[id][j].setMap(null);
		};
		//console.log("Remove " + tile);	
		delete tile_hashtable[id];
	};
	show_info = function(encoded_id){
		$.ajax({
			url: '{% url land_info %}',
			type : 'GET',
			data: encoded_id,
			dataType: 'html',
			success: function(data) {
				jQuery.facebox( data );
				/*
				$.liteDialog({
			        html: data,
			        width: '250px',
			    });
			    */
			},
		});
		
	};
	_get_loadedtiles = function(){
		var loadedtiles = [];
		$.each(tile_hashtable, function(id, tile){
			loadedtiles.push(id);
		});
		return loadedtiles;
	};
	return {
		show_waiting: function() {
			$.liteDialog( {width: '37px', heithg: '39px', html: $('#loading').html(),} );
		},
		end_waiting: function() {
			$.liteDialog('hide');
		},
		get_loadedtiles : function(){
			return _get_loadedtiles();
		},
		clear_all_tiles: function(){
			for (id in tile_hashtable){
			  remove_tile(id);
			};
		},
		remove_old_tiles: function(newtiles) {
			// Loop through newtiles hashtable
			// If tileid not exist in the newtiles param, remove
			for (id in tile_hashtable) {
				if ( ! newtiles[id]) {
					remove_tile(id);
				};
			};
			console.log("Loaded tiles after cleaned up: " + _get_loadedtiles().join(','))
		},
		add_new_tiles: function(newtiles) {
			var lands;
			var land;
			function attachListener(land, encoded_id){
				google.maps.event.addListener(land, 'click', function(event){
					show_info(encoded_id);
				});
			};
			// newtiles is a hashtable storing polyline arrays
			for (id in newtiles){
				lands = [];
				for (encoded_id in newtiles[id]){
					land = new google.maps.Polygon({
						paths: google.maps.geometry.encoding.decodePath(newtiles[id][encoded_id]),
						strokeColor: '#ff8300',
						strokeWeight: 2,
						fillColor: '#ff8300',
						fillOpacity: 0.35,
				    });
				    land.setMap(map);
					lands.push(land);
					attachListener(land, encoded_id);
				};
				if (lands.length>0){
					tile_hashtable[id] = lands;
				};
			};
			console.log("Loaded tiles after updated: " + _get_loadedtiles().join(','))
		},
	};
};

var manager = new LandManager();

$(document).ready( function () {

	//	alert("Your latitude: " + position.coords.latitude + "longitude: "
	// 		+ position.coords.longitude);
	
	var center = new google.maps.LatLng( {{ land.geometry.centroid.y }}, {{ land.geometry.centroid.x }} )
	var defaultView = true;
	var zoom = {{ zoom }};
	var gps = center;
	var bounds = null;
	var wpid = null;
	var image = new google.maps.MarkerImage('{{ STATIC_URL }}images/bluedotsmall.png');
	var bounds_stack = [];
	var refresh_tiles = function(){
		var bounds = bounds_stack.shift();
		if (bounds_stack.length == 0) {
			data = {
				bounds: map.getBounds(),
				zoom: map.getZoom(),
				loadedtiles: manager.get_loadedtiles(),
			};
			$.ajax({
				url: '{% url landws %}',
				type : 'GET',
				data: data,
				dataType: 'json',
				success: function(data) {
					manager.remove_old_tiles(data);
					manager.add_new_tiles(data);
				},
			});
		};
	};
    map = new google.maps.Map(document.getElementById('map'), {
        center: center,
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        disableDefaultUI: false,
    });
	var marker = new google.maps.Marker({
		icon: image,
	});
   	google.maps.event.addListener(map, 'bounds_changed', function() {
		bounds_stack.push(map.getBounds());
   		setTimeout(refresh_tiles, 1000);
   	});
	
	google.maps.event.addListener(map, 'zoom_changed', function(){
		bounds_stack.push(map.getBounds());
		setTimeout(function(){
			manager.clear_all_tiles();
			refresh_tiles();
		}, 1000);
		
	});
	
	$("#position").click(function(){
		if (defaultView) {
   			manager.show_waiting();
   			setTimeout(function(){
				wpid=navigator.geolocation.watchPosition(success = function(position){
					if (defaultView){
						// Zoom to GPS current position
						gps = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
						bounds = new google.maps.LatLngBounds();
						bounds.extend(gps);
						bounds.extend(center);
						map.fitBounds(bounds);
						marker.setPosition(gps);
						marker.setMap(map);
						defaultView = false;
						manager.end_waiting();
					};
				}, fail = function(error){
					manager.end_waiting();
				}, {enableHighAccuracy:true, maximumAge:30000, timeout:27000});
			}, 100);	
		}else{
			// Zoom back to default view
			navigator.geolocation.clearWatch(wpid);
			map.setZoom(zoom);
			map.panTo(center);
			marker.setMap(null);
			delete gps;
			delete marker;
			delete bounds;
			delete wpid;
			defaultView = true;
		}
	});
	
	$('#map').height( $(document).height());
	setTimeout(function() { window.scrollTo(0, 1); }, 200);
    
});
