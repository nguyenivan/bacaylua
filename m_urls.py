from django.conf.urls.defaults import *
from django.conf import settings
from django.views.generic.simple import direct_to_template
from pinax.apps.account.openid_consumer import PinaxConsumer

urlpatterns = patterns("",
	#url(r'^$', "datban.views.m_home", name = "m_home"),
	url(r'^$', 'datban.views.m_redirect' , name = 'redirect' ),
	url(r'^home/$', 'datban.views.m_home' , name = "home" ),
	url(r'^datban/lands/(?P<unit_path>.*?)/$', "datban.views.lands", name = "lands"),	
	url(r'^landws/$', "datban.views.landws", name = "landws"),
	url(r'^datban/land_info/$', "datban.views.land_info", name = "land_info"),
	url(r'^datban/bando/(?P<unit_path>.*?)$', "datban.views.map", name = "map"),
	url(r'^edit/$', 'datban.views.land_edit', name='land_edit'),
	url(r"^account/login/$", "pinax.apps.account.views.login", name="acct_login", kwargs={'template_name':'datban/m_login.html'}),
	url(r"^logout/$", "django.contrib.auth.views.logout", {"next_page": "/"}, name="acct_logout"),
	url(r"^openid/(.*)", PinaxConsumer()),
	url(r"^bbauth/", include("pinax.apps.bbauth.urls")),
	url(r"^authsub/", include("pinax.apps.authsub.urls")),
	url(r"^i18n/", include("django.conf.urls.i18n")),
	url(r"^avatar/", include("avatar.urls")),
	url(r"^avatar/", include("avatar.urls")),
	url(r"^profile/(?P<username>[\w\._-]+)/$", "datban.views.m_profile", name="profile_detail"),

)

if settings.SERVE_MEDIA:
    urlpatterns += patterns("",
        url(r"", include("staticfiles.urls")),
    )
