# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template.context import RequestContext
from datban.coordtrans import vn2000_to_wkt, epsg900913_vn2000_wkt
from django.shortcuts import render_to_response, get_object_or_404, redirect
from datban.models import AdminUnit, Land, ID_GAP
from profiles.models import Profile
from django.core import serializers
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse

from datban.polyline import get_more_polylines
from datban.forms import LandForm, LandFormStaff
import re, logging

logger = logging.getLogger(__name__)


from datban.utils import decode, encode
from django.contrib.gis.maps.google.zoom import GoogleZoom
import re,json

def m_profile(request, username):
	other_user = get_object_or_404(User, username=username)
	is_me = request.user.is_authenticated() and request.user == other_user
	lands = other_user.land_set.all()
	return render_to_response('datban/m_lands.html', dict({'lands': lands, 'is_me': is_me,}), 
														context_instance=RequestContext(request))
def m_redirect(request):
	if request.user.is_authenticated():
		return redirect('profile_detail', username = request.user.username)
	else:
		return redirect('home')
	
def m_home(request):
	return render_to_response('datban/m_home.html', context_instance=RequestContext(request))

def home (request):
	users = User.objects.filter(profile__admin_unit__name = u'Thành Phố Hồ Chí Minh')
	return render_to_response('datban/datbanhome.html',
							dict ({'users':users,}),
							context_instance=RequestContext(request))
	
def coordtrans (request):
	if request.is_ajax and request.method == 'GET':
		wkt = request.GET.get ('wkt')
		if wkt is not None: 
			converted = vn2000_to_wkt (wkt)
			return HttpResponse (converted, mimetype = 'text/plain')

	#All else return 404
	raise Http404

def reversecoordtrans (request):
	if request.is_ajax and request.method == 'GET':
		wkt = request.GET.get ('wkt')
		if wkt is not None: 
			converted = epsg900913_vn2000_wkt (wkt)
			return HttpResponse (converted, mimetype = 'text/plain')

	#All else return 404
	raise Http404

@login_required
def land_edit(request, form_class=LandForm, *args, **kwargs):
	encoded_id = request.GET.get('encoded_id')
	last_page = request.GET.get('last_page')
	template_name = kwargs.get(
	    "template_name_facebox",
	    "datban/land_edit_facebox.html"
	)
	if request.user.is_staff:
		form_class=LandFormStaff
	if not last_page or not encoded_id:
		raise Http404
	id = decode(str(encoded_id)) - ID_GAP
	land = get_object_or_404(Land, id=id )
	if  not request.user.is_staff and land.user != request.user:
		raise Http404
	if request.method == "POST":
		#last_page = request.POST.get('last_page','/')
		land_form = form_class(request.POST, instance=land)
		if land_form.is_valid():
			land.save()
			return HttpResponse(last_page, mimetype='text/plain')
	else:
		land_form = form_class(instance=land)
	return render_to_response(template_name, {
		"land": land,
		"land_form": land_form,
		"last_page": last_page,
	}, context_instance=RequestContext(request))

## Return lands related to the unit path
#  @param unit_path The path to the Administrative Unit. For example thanhphohochiminh/huyenbinhchanh/xaphongphu
def lands (request, unit_path):
	from django.db.models import Q
	unit = AdminUnit.objects.get(path = unit_path)
	if unit is None:
		raise Http404
	descendants = []
	children = [unit]
	while children:
		descendants.extend(children)
		temp = []
		for child in children:
			if child.subordinates:
				for u in child.subordinates.all():
					temp.append(u)
		children = temp
	unit_filter = None
	for u in descendants:
		unit_filter = unit_filter | Q(admin_unit=u) if unit_filter else Q(admin_unit=u) 
	
	sort = request.GET.get('sort', 'listed')
	from django.db.models import Q
	private_filter = Q(is_private = False)
	if request.user.is_authenticated():
		private_filter = private_filter | Q(user = request.user)	
	
	if sort == 'all':
		# Select all related lands
		lands = Land.objects.filter(private_filter).filter(unit_filter)
	elif sort == 'sold':
		lands = Land.objects.filter(listing_status = 3).filter(private_filter).filter(unit_filter)
	else:
		lands = Land.objects.filter(listing_status = 1).filter(private_filter).filter(unit_filter)
		sort = 'listed'
			
	current_site = Site.objects.get_current()
	if current_site.domain == 'b.bacaylua.com':
		return render_to_response('datban/lands.html',
								dict ({'unit':unit, 'lands':lands, 'sort': sort,}),
								context_instance=RequestContext(request))
	elif current_site.domain == 'm.bacaylua.com':
		return render_to_response('datban/m_lands.html',
								dict ({'unit':unit, 'lands':lands, 'sort': sort,}),
								context_instance=RequestContext(request))

## Return a map which high light the specified land if specified. Otherwise limit the map view to specified unit admin.
#  @param map_path Example 	thanhphohochiminh/huyenbinhchanh/xaphongphu/#xyz 	view land xyz ( xyz = datban.utils.decode(id+1000000000)
#  					or 		thanhphohochiminh/huyenbinhchanh/xaphongphu			view all lands in Phong Phu ward
#  Sample map_path: /datban/bando/thanhphohochiminh/huyennhabe#QUIP
def map (request, unit_path):
	 # (?P<unit_path>.+)?(?P<encoded_id>[\w_-]+$)', 'thanhphohochiminh/huyenbinhchanh/xaphongphu/#xyz__-12
	encoded_id = request.META['QUERY_STRING']
	try:
		id = decode (encoded_id) - ID_GAP
	except: # Cannot decode this id
		raise Http404
	
	#lookup and validate
	land = Land.objects.get(pk = id)
	if land is None:
		raise Http404
	if land.admin_unit and land.admin_unit.path != unit_path:
		raise Http404
	
	#return the view
	current_site = Site.objects.get_current()
	if current_site.domain == 'b.bacaylua.com':
		return render_to_response('datban/map.html', dict({'land_id': id, 'land': land, 'zoom': GoogleZoom().get_zoom( land.geometry), }),
							context_instance = RequestContext(request))
	elif current_site.domain == 'm.bacaylua.com':
		return render_to_response('datban/m_map.html', dict({'land_id': id, 'land': land, 'zoom': GoogleZoom().get_zoom( land.geometry), }),
							context_instance = RequestContext(request))
	 
	 
def landws (request):
	#if request.is_ajax and request.method == 'GET':
	loadedtiles = request.GET.getlist('loadedtiles')
	bounds_str = request.GET.get('bounds')
	zoom = int(request.GET.get('zoom'))
	bounds = [float(coord) for coord in (re.findall('[\d\.]+', bounds_str)) ]
	more_tiles = get_more_polylines(bounds,loadedtiles,zoom)
	
	return HttpResponse (json.dumps(more_tiles), mimetype ='application/json')
	#return HttpResponse (json.dumps(more_tiles), mimetype ='text/plain')
	# All else return 404
	#raise Http404

def land_info(request):
	#the request is in the format http://bacaylua.com/datban/land_info/?[encoded_id]
	try:
		encoded_id = request.META['QUERY_STRING']
		try:
			id = decode (encoded_id) - ID_GAP
		except: # Cannot decode this id
			raise Http404
		land = Land.objects.get(pk = id)
		if land is None:
			raise Http404
		unit_path = '%s?%s' % ( land.admin_unit.path, encoded_id)
		return render_to_response ('datban/land_info.html', dict(
																{'land': land, 
																'last_page': reverse('map', args=[unit_path]),}
																), context_instance = RequestContext(request) )  	
	
	except :
		import traceback
		traceback.print_exc()
		
		
