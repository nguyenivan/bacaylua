# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _
import datetime, os, re, traceback
from django.core.urlresolvers import reverse
from django.contrib.gis.maps.google.overlays import GPolyline
from utils import encode
from django.db import transaction

ID_GAP = 1000000


#from django.core.management.validation import max_length
#from twisted.python.util import blank
#from django.contrib.admin.util import limit_choices_to, related_name

from datban import utils


class AdminUnit (models.Model):
    
	LEVEL_CHOICES = (
		(1, _('Coutry')),
		(2, _('Province')),
		(3, _('District')),
		(4, _('Ward')),
	)
    
	name = models.CharField(_('Name'), max_length = 50)
	geometry = models.MultiPolygonField(_('Geographic Border'),srid=4326)
	level = models.IntegerField( choices = LEVEL_CHOICES, default = 2) #province, district, ward
	superordinate = models.ForeignKey('self', blank = True, null = True, related_name = 'subordinates')
	slug = models.CharField(_('Slug'), max_length=50)
	path = models.CharField(_('Unit Path'), max_length=100, blank = True)
	markup = models.CharField( _('Http Markup'), max_length = 500, blank = True)
	objects = models.GeoManager()
	sub_order = models.IntegerField(_('Order'))
	class Meta:
		ordering = ['sub_order', 'name']
		
	def __unicode__(self):
		unit = self
		names = list()
		while unit and unit.level > 1: 
			names.append(unit.name)
			unit = unit.superordinate
		return os.path.sep.join(names)
	
	def save(self, *args, **kwargs):
		''' On save, Unit Path and Http Markup '''
		
		#Create list of superordinate
		supers = list()
		slugs = list()
		unit = self
		#Auto generate slug if not set
		if not unit.slug or not unit.slug.strip():
			unit.slug = re.sub(r'\s', '', utils.remove_accent(unit.name.encode('utf8')).lower())
		#Select all super except Vietnam
		while unit and unit.level > 1: 
			supers.append(unit)
			slugs.append(unit.slug)
			unit = unit.superordinate
		supers.reverse()
		slugs.reverse()		
		self.path = os.path.sep.join(slugs)
        
		links = list()         
		for unit in supers:
			url = reverse('lands',  args = [unit.path])
			link = u'<a href="%s">%s</a>' % (url, unit.name)
			links.append(link)
		self.markup = os.path.sep.join(links)	
		super(AdminUnit, self).save(*args, **kwargs)


class Land (models.Model):
    
	LEGAL_STATUS_CHOICES = (
		(1, _('Red Paper')),
		(2, _('Pink Paper')),
		(3, _('Hand Written Paper')),
	)
	
	LAND_TYPE_CHOICES = (
		(1, _('Residence')),
		(2, _('House')),
		(3, _('Agricultural')),
		(4, _('Agricultural + Residence')),
	)
	
	LISTING_STATUS_CHOICES = (
		(1, _('For Sales')),
		(2, _('Need To Buy')),
		(3, _('Sold')),
	)
	user = models.ForeignKey(User, verbose_name=_('user'))
	geometry = models.MultiPolygonField(_('Land Border'),srid=4326)
	price = models.BigIntegerField(_('Listed Price'), blank = True, null = True) #unit: million VND
	area = models.FloatField(_('Total Area (m2)')) #unit: m2 (square meter)
	legal_status = models.IntegerField(_('Legal Status'), choices = LEGAL_STATUS_CHOICES, default = 1) # Sổ Đỏ, Sổ Hồng, Giấy Tay
	land_type = models.IntegerField(_('Land Type'), choices = LAND_TYPE_CHOICES, default = 1)# Nhà Ở, Thổ Cư, Nông Nghiệp, Nông Nghiệp + Thổ Cư
	marker = models.PointField(_('Position'), srid=4326, blank = True, null = True)
	owner_name = models.CharField(_("Owner's Name"), max_length = 50, blank = True)
	admin_unit = models.ForeignKey(AdminUnit, verbose_name = _('Administrative Unit'), blank = True, null= True)
	listing_status = models.IntegerField(_('Listing Status'), choices = LISTING_STATUS_CHOICES, default = 1)  #selling, buing, sold
	created_on = models.DateTimeField(_('Created On'), blank = True)
	last_modified = models.DateTimeField(_('Last Modified'), blank = True)
	description = models.TextField(_('Description'))
	contact_phone = models.CharField(_('Contact Phone'), max_length = 20)
	contact_name =  models.CharField(_('Contact Name'), max_length = 50)
	static_map = models.URLField(_('Static Map'), max_length = 2000, blank = True)
	objects = models.GeoManager()
	is_private = models.BooleanField(_('Private Listing'), default = False)
	
	class Meta:
		ordering = ['-last_modified']	
		
    ## Generate static google map from multipolygon url
    #  @example http://maps.googleapis.com/maps/api/staticmap?path=color:0xff0000ff|weight:5|40.737102,-73.990318|40.749825,-73.987963|40.752946,-73.987384|40.755823,-73.986397&size=512x512&sensor=false
	def generate_static_map(self):
		if self.geometry is None:
			return None
		# The Geometry's WKT will look like this MULTIPOLYGON (((106.6786154212305036 10.6854475768920061, 106.6789901199219770 10.6854283820627280, 106.6788799962620260 10.6852840635758657, 106.6785783928939253 10.6852940006168762, 106.6786154212305036 10.6854475768920061)))
		import re
		self.geometry.transform(4326)
		matches = re.findall('(\([\d\s\,\.]*\))', self.geometry.wkt)
		polylines = list()
		for match in matches:
			#Convert WKT to google stuff
			 vertices = re.findall( '(?P<x>\d+\.\d+)\s+(?P<y>\d+\.\d+)' , match.strip('()') )
			 ll_list = list()
			 for vertex in vertices:
			 	#x is lon, y is lat
			 	ll_list.append('%s,%s' % (vertex[1], vertex[0]))
			 polyline = 'path=color:0xff8300ff|%s' % ( '|'.join(ll_list), )
			 polylines.append(polyline)
		#http://maps.googleapis.com/maps/api/staticmap?maptype=satellite&size=160x160&sensor=false&path=color:0xff8300ff|10.6854475768919279,106.6786154212305178|10.6854283820627138,106.6789901199219486|10.6852840635758000,106.6788799962619976|10.6852940006168620,106.6785783928939537|10.6854475768919279,106.6786154212305178	 
		url = 'http://maps.googleapis.com/maps/api/staticmap?maptype=satellite&size=160x160&sensor=false&%s' % ( '&'.join(polylines), )
		return url
	
	def get_admin_unit(self):
		ou = AdminUnit.objects.filter(geometry__intersects=self.geometry)
		max_level =0
		max_area=0
		max = None
		for u in ou:
			g =  self.geometry.intersection(u.geometry)
			g.transform(900913)
			if (u.level == max_level and g.area > max_area) or u.level > max_level:
				max_level = u.level
				max_area = g.area
				max=u
		return max
	
	def full_map_url(self):
		return '/datban/bando/%s?%s' % (self.admin_unit.path, encode(self.id + ID_GAP))
	
	def save(self, *args, **kwargs):
		''' On save, update timestamps '''
		if not self.id:
		    self.created_on = datetime.datetime.today()
		if not kwargs.get('keep_date'):
			self.last_modified = datetime.datetime.today()
		self.static_map = self.generate_static_map()
		self.admin_unit = self.get_admin_unit()
		super(Land, self).save()

	def gpoly(self):
		gpoly = GPolyline(self.geometry[0])
		return gpoly.latlngs.replace('GLatLng', 'google.maps.LatLng')
	
class Invitation(models.Model):
	'An invitation keeps a ticket which will be expired after a specific hours (default is 24 hours)'
	ticket = models.CharField(max_length = 20, blank = False)
	expired_after = models.IntegerField(_('Expired After'), default = 24)
	created_on = models.DateTimeField(_('Created On'), auto_now_add = True, blank = True)
	land = models.ForeignKey(Land, verbose_name = _('Land'), blank = False)	
	
class ThuaDiaChinh(models.Model):
	wkb_geometry = models.GeometryField(srid=4326)
	idthua = models.CharField(max_length = 10, blank = True)
	soto = models.CharField(max_length = 20, blank = True)
	sothua = models.CharField(max_length = 20, blank = True)
	dientich = models.FloatField(blank = True)
	tennha = models.CharField(max_length = 100, blank = True)
	tenchusohuu = models.CharField(max_length = 100, blank = True)
	sonha = models.CharField(max_length = 50, blank = True)
	sonhamoi = models.CharField(max_length = 50, blank = True)
	maduong = models.CharField(max_length = 20, blank = True)
	maduongmoi = models.CharField(max_length = 20, blank = True)
	maphuong = models.CharField(max_length = 7, blank = True)
	maquan = models.CharField(max_length = 5, blank = True)
	idconduong = models.CharField(max_length = 10, blank = True)
	tenconduong = models.CharField(max_length = 100, blank = True)
	objects = models.GeoManager()	

class HcmDistrict(models.Model):
	code = models.CharField(max_length=12)
	name = models.CharField(max_length=50)
	geometry = models.GeometryField(srid=4326)
	objects = models.GeoManager()

class HcmWard(models.Model):
	code = models.CharField(max_length=12)
	name = models.CharField(max_length=50)
	geometry = models.GeometryField(srid=4326)
	district_code = models.CharField(max_length=50)
	objects = models.GeoManager()
	
	
	
	
	
	