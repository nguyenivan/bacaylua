# -*- coding: utf-8 -*-


from django.db import transaction
import traceback	
@transaction.commit_manually		
def a():
	from datban.models import Land, AdminUnit
	try:
		ol = Land.objects.all().order_by('id')
		for l in ol:
			if l.user.username == 'nguyenivan':
				print l.id, l.contact_name, l.contact_phone
				l.contact_name = u'Anh Nguyên'
				l.contact_phone = '01635970520'
				l.save()
	except Exception, e:
		print "Rolled back."
		transaction.rollback()
		traceback.print_exc()
		raise e
	else:
		transaction.commit()



@transaction.commit_manually
def convert_thua(commit = False, max_count = None, keep_below = None):
	from datban.models import ThuaDiaChinh, Land, AdminUnit
	from django.contrib.gis.geos import MultiPolygon, GEOSGeometry
	from django.contrib.auth.models import User
	from random import choice
	from exceptions import Exception
	from datban.coordtrans import vn2000_to_geom, m_to_d
	import time
	import sys
	if keep_below is None:
		raise Exception('Must specify keep_below')
	#max_convert=10
	try:
		# Delete all sold lands under NguyenIvan username
		lands = Land.objects.filter(listing_status =3).filter(id__gt=keep_below)
		num_del = lands.count()
		if commit:
			lands.delete()
		print 'Total %d lands with id > %d was cleared. Now update thua dia chinh ...' % (num_del, keep_below,)	
	except Exception ,e:
		transaction.rollback()
		traceback.print_exc()
		raise e
	else:
		transaction.commit()
	
	# Then convert ThuaDiaChinh to sold lands under NguyenIvan username
	try:
		thuas = ThuaDiaChinh.objects.all()
		old_string = None
		new_string = None
		counter = 0
		for (total,thua) in enumerate(thuas):
			mp = GEOSGeometry(vn2000_to_geom(thua.wkb_geometry.wkt).ExportToWkt(), srid = 4326)
			area = thua.wkb_geometry.area
			if thua.wkb_geometry.num_coords<=10 and thua.wkb_geometry.area <= 1000 and thua.wkb_geometry.area >= 50 and choice(range(4)) == 0:
				land = Land(
						user = User.objects.get(username='nguyenivan') ,
						area = area,
						owner_name = thua.tenchusohuu if thua.tenchusohuu is not None else '',
						listing_status = 3,
						contact_phone = '01635970520',
						contact_name = u'Anh Nguyên',
						is_private = False,
						geometry = mp,
						price = 0,
				)
				if commit:
					land.save()
				counter +=1
				if (counter +1) % 100 == 0:
					new_string = '%d out of %d lands updated' % (counter +1, total +1)
				else:
					new_string = 'Updating land %d which has %d vertex in %d m2' % (thua.id, thua.wkb_geometry.num_coords, thua.wkb_geometry.area)
			else:
				new_string = 'Skipping land %d because which has %d vertex' % (thua.id, mp.num_coords)

			if old_string:
				sys.stdout.write('\b' * len(old_string))

			sys.stdout.write(new_string)
			sys.stdout.flush()
			old_string = new_string
			if max_count and counter > max_count:
				break
			if (counter +1) % 100 == 0: time.sleep(0.5)			
			
	except Exception ,e:
		transaction.rollback()
		print ('=' * 20)
		print ('%d lands was about to be saved, error at thua id: %d' % (counter, thua.id))
		print ('Transaction was rolled back, no land was updated.')
		print ('=' * 20)
		traceback.print_exc()
		raise e
	else:
		transaction.commit()
		print('')
		print ('=' * 20)
		print ('TOTAL: %d out of %d lands was updated to the database under NguyenIvan username.' % (counter + 1, total +1))
		print ('=' * 20)
		
@transaction.commit_manually
def populate_hcmward():
	import re
	from django.contrib.gis.geos import MultiPolygon, GEOSGeometry
	from coordtrans import vn2000_to_geom
	district_codes = {
			u"Th? Ð?c" : 48,
			u"T?n Ph?" : 36,
			u"T?n B?nh" : 28,
			u"Ph? Nhu?n" : 31,
			u"Gu V?p" : 29,
			u"B?nh Th?nh" : 30,
			u"9" : 47,
			u"8" : 39,
			u"7" : 40,
			u"6" : 37,
			u"5" : 35,
			u"4" : 38,
			u"3" : 32,
			u"2" : 49,
			u"12" : 43,
			u"11" : 34,
			u"10" : 33,
			u"1" : 27,
			u"Nhà B`" : 46,
			u"Hac M?n" : 50,
			u"C? Chi" : 44,
			u"C?n Gi?" : 45,
			u"B?nh T?n" : 42,
			u"B?nh Ch?nh" : 41,
	}
	
	try:
		from datban.models import AdminUnit, HcmDistrict, HcmWard
		wards = HcmWard.objects.all()
		for (counter, ward) in enumerate(wards):
			district = AdminUnit.objects.get(pk = district_codes[ward.district_code])
			#mp=GEOSGeometry(vn2000_to_geom(district.geometry.wkt).ExportToWkt())
			mp=ward.geometry
			unit = AdminUnit(
							name=ward.name, 
							geometry=mp, 
							level = 4, 
							superordinate = district, 
							slug=unicode(re.sub(r'\s', '', remove_accent(ward.name.encode('utf8').lower())), 'utf8')
							)
			print "Saving ", unit.name, unit.slug, district.name
			print unit.geometry.wkt
			unit.save()
	except Exception, e:
		transaction.rollback()
		print ('=' * 20)
		print ('Transaction was rolled back, no admin unit was added.')
		print ('=' * 20)
		raise e
	else:
		transaction.commit()
		print('')
		print ('=' * 20)
		print ('TOTAL: %d admin units was added to the database.' % (counter + 1,) )
		print ('=' * 20)

@transaction.commit_manually
def populate_hcmdistrict():
	import re
	from django.contrib.gis.geos import MultiPolygon, GEOSGeometry
	from coordtrans import vn2000_to_geom
	try:
		from datban.models import AdminUnit, HcmDistrict, HcmWard
		hcm = AdminUnit.objects.get(path="thanhphohochiminh")
		districts = AdminUnit.objects.filter(superordinate=hcm)
		'''
		for district in districts:
			if district.id not in [1,2]:
				print "Deleting ", district.name
				district.delete()
		print "All district just added has been deleted"
		'''
		districts = HcmDistrict.objects.all()
		for (counter, district) in enumerate(districts):
			mp=GEOSGeometry(vn2000_to_geom(district.geometry.wkt).ExportToWkt())
			unit = AdminUnit(
							name=district.name, 
							geometry=mp, 
							level = 3, 
							superordinate = hcm, 
							slug=unicode(re.sub(r'\s', '', remove_accent(district.name.encode('utf8').lower())), 'utf8')
							)
			print "Saving ", unit.name, unit.slug
			unit.save()
	except Exception, e:
		transaction.rollback()
		print ('=' * 20)
		print ('Transaction was rolled back, no admin unit was added.')
		print ('=' * 20)
		raise e
	else:
		transaction.commit()
		print('')
		print ('=' * 20)
		print ('TOTAL: %d admin units was added to the database.' % (counter + 1,) )
		print ('=' * 20)
		
from datban.models import AdminUnit
def update_binhchanh(commit=False):
	new_names = {
		811:u"Xã Quy Đức",
		812:u"Xã Bình Chánh",
		813:u"Xã Tân Quý Tây",
		814:u"Xã Hưng Long",
		815:u"Thị Trấn Tân Túc",
		816:u"Xã An Phú Tây",
		817:u"Xã Phong Phú",
		818:u"Xã Tân Kiên",
		819:u"Xã Bình Hưng",
		820:u"Xã Tân Nhựt",
		821:u"Xã Lê Minh Xuân",
		822:u"Xã Bình Lợi",
		823:u"Xã Vĩnh Lộc B",
		824:u"Xã Vĩnh Lộc A",
		825:u"Xã Phạm Văn Hai",
		826:u"Xã Đa Phước",
	}
	ou = AdminUnit.objects.all()
	for u in ou:
		if u.id in new_names:
			print "Updating ", u.id
			u.name=new_names[u.id]
			u.slug=None
			if commit:
				u.save()
			print u.id, u.name, u.path, u.slug

from datban.models import AdminUnit
def update_tanbinh(commit=False):
	new_names = {
		528:u"Phường 9",
		529:u"Phường 10",
		530:u"Phường 6",
		531:u"Phường 8",
		532:u"Phường 7",
		533:u"Phường 5",
		534:u"Phường 11",
		535:u"Phường 3",
		536:u"Phường 1",
		537:u"Phường 14",
		538:u"Phường 13",
		539:u"Phường 12",
		540:u"Phường 4",
		541:u"Phường 2",
		542:u"Phường 15",
	}
	ou = AdminUnit.objects.all()
	for u in ou:
		if u.id in new_names:
			print "Updating ", u.id
			u.name=new_names[u.id]
			u.slug=None
			if commit:
				u.save()
			print u.id, u.name, u.path, u.slug
			

