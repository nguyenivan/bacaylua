# admin.py

# This file controls the look and feel of the models within the Admin App
# They appear in the admin app once they are registered at the bottom of 
# this code (same goes for the databrowse app)

from django.conf import settings # needed if we use the GOOGLE_MAPS_API_KEY from settings

# Import the admin site reference from django.contrib.admin
from django.contrib import admin

# Grab the Admin Manager that automaticall initializes an OpenLayers map
# for any geometry field using the in Google Mercator projection with OpenStreetMap basedata
from django.contrib.gis.admin import OSMGeoAdmin, GeoModelAdmin

# Note, another simplier manager that does not reproject the data on OpenStreetMap is available
# with from `django.contrib.gis.admin import GeoModelAdmin`

# Finally, import our model from the working project
# the geographic_admin folder must be on your python path
# for this import to work correctly
from datban.models import Land, AdminUnit

# Import the Databrowse app so we can register our models to display via the Databrowse
# from django.contrib import databrowse
# databrowse.site.register(WorldBorders)

USE_GOOGLE_TERRAIN_TILES = True

class AdminUnitAdmin(OSMGeoAdmin):
	list_display = ('name', 'level', 'superordinate',)
	search_fields = ('name', )
	list_per_page = 10
	save_as = True
	list_select_related = True

	map_template = 'gis/admin/datban.html'
	display_wkt = False
	display_srid = True
	num_zoom = 20
	default_zoom = 12
	map_srid = 900913
	default_lon = 11875533
	default_lat = 1198797
	scrollable = False
	#admin_media_prefix = settings.ADMIN_MEDIA_PREFIX
	map_height = 600
	map_width = 800
		
class LandAdmin(OSMGeoAdmin):
	"""
	
	The class that determines the display of the WorldBorders model
	within the Admin App.
	
	This class uses some sample options and provides a bunch more in commented
	form below to show the various options GeoDjango provides to customize OpenLayers.
	
	For a look at all the GeoDjango options dive into the source code available at:
	
	http://code.djangoproject.com/browser/django/trunk/django/contrib/gis/admin/options.py
	
	"""
	
	# Standard Django Admin Options
	list_display = ('user', 'area', 'land_type', 'listing_status', 'contact_phone', 'geometry',)
	list_editable = ('geometry',)
	search_fields = ('description', 'land_type',)
	list_per_page = 10
	#ordering = ('last_modified',)
	list_filter = ('user', 'price',  'area', 'land_type', 'listing_status',)
	save_as = True
	list_select_related = True
	fieldsets = (
	  ('Listing Information', {'fields': ('is_private', 'area','price', 'land_type' , 'legal_status', 'listing_status', 'description', 'created_on',), 'classes': ('show','extrapretty')}),
	  ('Detail', {'fields': ('user','owner_name', 'admin_unit', 'last_modified', ), 'classes': ('show',)}),
	  ('Contact Information', {'fields': ('contact_phone','contact_name',), 'classes': ('show', 'wide')}),
	  ('Position and Border', {'fields': ('geometry', ), 'classes': ('show', 'wide')}),
	)
	
	display_wkt = False
	display_srid = True
	num_zoom = 20
	default_zoom = 12
	map_srid = 900913
	default_lon = 11875533
	default_lat = 1198797
	
	if USE_GOOGLE_TERRAIN_TILES:
		map_template = 'gis/admin/datban.html'
		extra_js = ['http://maps.google.com/maps?file=api&amp;v=2&amp;key=%s' % settings.GOOGLE_MAPS_API_KEY]
	else:
		pass
	# Default GeoDjango OpenLayers map options
	# Uncomment and modify as desired
	# To learn more about this jargon visit:
	# www.openlayers.org
	
	#default_lon = 0
	#default_lat = 0
	#default_zoom = 4
	#display_wkt = False
	#display_srid = False
	#extra_js = []
	#num_zoom = 18
	#max_zoom = False
	#min_zoom = False
	#units = False
	#max_resolution = False
	#max_extent = False
	#modifiable = True
	#mouse_position = True
	#scale_text = True
	#layerswitcher = True
	scrollable = False
	#admin_media_prefix = settings.ADMIN_MEDIA_PREFIX
	map_height = 300
	map_width = 400
	#map_srid = 4326
	#map_template = 'gis/admin/openlayers.html'
	#openlayers_url = 'http://openlayers.org/api/2.6/OpenLayers.js'
	#wms_url = 'http://labs.metacarta.com/wms/vmap0'
	#wms_layer = 'basic'
	#wms_name = 'OpenLayers WMS'
	#debug = False
	#widget = OpenLayersWidget

# Finally, with these options set now register the model
# associating the Options with the actual model
admin.site.register(Land,LandAdmin)
admin.site.register(AdminUnit, AdminUnitAdmin)