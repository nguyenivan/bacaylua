from django import template
from datban.utils import encode
from datban.models import ID_GAP
from django.core.urlresolvers import reverse

register = template.Library()

@register.inclusion_tag("land_info.html", takes_context=True)
def land_info(context):
    return {
		"land": context['land'],
		"user": context['user'],
		"request": context['request'],
	}

from datban.utils import url_with_querystring
@register.simple_tag
def url_land_edit(land, last_page):
	return url_with_querystring(reverse('land_edit'), encoded_id = encode(land.id + ID_GAP), last_page = last_page)
	#return reverse('land_edit', args = [encode(land.id + ID_GAP), last_page] )
    