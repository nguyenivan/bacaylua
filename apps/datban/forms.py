from django import forms
from datban.models import Land

class LandFormStaff(forms.ModelForm):
    class Meta:
        model = Land
        exclude = ('geometry', 'marker', 'admin_unit', 'created_on', 'last_modified', 'static_map')

class LandForm(forms.ModelForm):
    class Meta:
        model = Land
        exclude = ('user', 'geometry', 'marker', 'admin_unit', 'created_on', 'last_modified', 'static_map')
