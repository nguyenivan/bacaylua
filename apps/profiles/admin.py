from django.contrib import admin
from profiles.models import Profile
from django.contrib.auth.models import User
from avatar.models import Avatar
from emailconfirmation.models import EmailAddress


class UserProfileInline(admin.StackedInline):
    model = Profile
    max_num = 1
    can_delete = True
    fields = ('name', 'about', 'admin_unit')

class UserAvatarInline(admin.StackedInline):
    model = Avatar
    max_num = 1
    can_delete = True

class UserEmailAddressInline(admin.StackedInline):
    model = EmailAddress
    max_num = 10
    can_delete = True


class UserAdmin(admin.ModelAdmin):
    inlines = [ UserProfileInline, UserAvatarInline, UserEmailAddressInline, ]
    fields = ('username', 'password', 'is_staff', 'is_active', 'is_superuser')
    
    

# unregister old user admin
admin.site.unregister(User)
# register new user admin
admin.site.register(User, UserAdmin)